#!/bin/bash
#
# WARNING: This overwrites the bootloader on the eMMC rescue disk!
#
# This u-boot version expects /Image and /imx8mq-mnt-reform2.dtb
# on /dev/mmcblk0p1
#
sudo dd if=flash-rescue.bin of=/dev/mmcblk0boot0 bs=1024 seek=33
sync

